﻿using System;

namespace IDAL.Models
{
    public class CommandResult
    {
        public Guid Id { get; set; }
        public DateTime EventTime { get; set; }
        public TimeSpan PerformTime { get; set; }
        public string Command { get; set; }
        public bool Result { get; set; }
        public string OutputInformation { get; set; }
    }
}