﻿namespace IDAL
{
    public class WorkResult
    {
        #region CTOR

        public WorkResult(bool success, string outputMessage)
        {
            if (string.IsNullOrEmpty(outputMessage))
            {
                outputMessage = "Work succeeded";
            }
            ;
            Succeeded = success;
            OutputMessage = outputMessage;
        }


        public WorkResult(string outputMessage)
        {
            if (string.IsNullOrEmpty(outputMessage))
            {
                outputMessage = "DefaultError";
            }
            Succeeded = false;
            OutputMessage = outputMessage;
        }

        #endregion

        #region Fields

        public bool Succeeded { get; private set; }
        public string OutputMessage { get; private set; }

        #endregion

        #region Static methods

        public static WorkResult Failed(string errors = null)
        {
            return new WorkResult(errors);
        }

        public static WorkResult Success(string outputMessage = null)
        {
            return new WorkResult(true, outputMessage);
        }

        #endregion
    }
}