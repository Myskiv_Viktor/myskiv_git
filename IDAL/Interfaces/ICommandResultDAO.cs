﻿using System.Collections.Generic;
using System.Threading.Tasks;
using IDAL.Models;

namespace IDAL.Interfaces
{
    public interface ICommandResultDAO
    {
        Task<WorkResult> AddNewResult(CommandResult result);

        Task<List<CommandResult>> GetLast20Result();
    }
}