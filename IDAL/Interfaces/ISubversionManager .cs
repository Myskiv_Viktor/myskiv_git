﻿using System.Threading.Tasks;

namespace IDAL.Interfaces
{
    public interface ISubversionManager
    {
        Task<WorkResult> InvokeCommand(string command, string folderPath);
    }
}