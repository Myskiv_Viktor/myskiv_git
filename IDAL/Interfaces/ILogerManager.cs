﻿using System.Collections.Generic;
using System.Threading.Tasks;
using IDAL.Models;

namespace IDAL.Interfaces
{
    public interface ILogerManager
    {
        Task<WorkResult> AddNewResult(CommandResult commandResult);
        Task<List<CommandResult>> GetLast20Result();
    }
}