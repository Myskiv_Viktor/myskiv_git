﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Web.Mvc;
using IDAL;
using IDAL.Interfaces;
using IDAL.Models;
using Myskiv_Git.ViewModel;

namespace Myskiv_Git.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogerManager _loger;
        private readonly ISubversionManager _subversionManager;

        public HomeController(ISubversionManager subversionManager, ILogerManager loger)
        {
            _subversionManager = subversionManager;
            _loger = loger;
        }

        [HttpGet]
        public async Task<ActionResult> Index()
        {
            ControlPanelViewModel view = new ControlPanelViewModel();
            List<CommandResult> commandResults = await _loger.GetLast20Result();
            if (commandResults.Count > 0)
            {
                view.CommandResults = commandResults;
            }
            return View(view);
        }

        [HttpPost]
        public async Task<ActionResult> Index(ControlPanelViewModel view)
        {
            if (ModelState.IsValid)
            {
                var watch = Stopwatch.StartNew();
                WorkResult workResult = await _subversionManager.InvokeCommand(view.CommandLine, view.FolderPath);
                watch.Stop();

                CommandResult commandResult = new CommandResult
                {
                    Command = view.CommandLine,
                    EventTime = DateTime.Now,
                    PerformTime = watch.Elapsed,
                    Id = Guid.NewGuid(),
                    OutputInformation = workResult.OutputMessage,
                    Result = workResult.Succeeded
                };
                await _loger.AddNewResult(commandResult);

                List<CommandResult> commandResults = await _loger.GetLast20Result();
                if (commandResults.Count > 0)
                {
                    view.CommandResults = commandResults;
                    view.OutputInfo = workResult.OutputMessage;
                }
            }
            return View(view);
        }
    }
}