﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using IDAL.Models;

namespace Myskiv_Git.ViewModel
{
    public class ControlPanelViewModel
    {
        public ControlPanelViewModel()
        {
            CommandResults = new List<CommandResult>();
        }

        [Required(ErrorMessage = "You must fill in field")]
        public string CommandLine { get; set; }

        [Required(ErrorMessage = "You must fill in field")]
        public string FolderPath { get; set; }

        public string OutputInfo { get; set; }

        public List<CommandResult> CommandResults { get; set; }
    }
}