﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using IDAL;
using IDAL.Interfaces;

namespace BLL.Services
{
    public class GitManager : ISubversionManager
    {
        private static readonly string GitPath;
        private static readonly string RootFolder;
        private static readonly ProcessStartInfo processStartInfo;

        static GitManager()
        {
            GitPath = ConfigurationManager.AppSettings["gitPath"];
            RootFolder = ConfigurationManager.AppSettings["pathToTootFolder"];
            processStartInfo = new ProcessStartInfo
            {
                WindowStyle = ProcessWindowStyle.Hidden,
                CreateNoWindow = true,
                FileName = GitPath,
                UseShellExecute = false,
                RedirectStandardInput = true,
                RedirectStandardOutput = true,
                RedirectStandardError = true
            };
        }

        public Task<WorkResult> InvokeCommand(string command, string folderPath)
        {
            if (!string.IsNullOrEmpty(command) && !string.IsNullOrEmpty(folderPath))
            {
                // Try to create user folder in root catalog
                try
                {
                    string path = Path.GetFullPath(RootFolder + folderPath);
                    Directory.CreateDirectory(path);

                    // Save argument for opening exe 
                    processStartInfo.Arguments = $"--cd={path}";
                }
                catch (Exception ex)
                {
                    return Task.FromResult(WorkResult.Failed($"The process failed: {ex}"));
                }

                // Execute git command
                using (Process gitProcess = Process.Start(processStartInfo))
                {
                    using (StreamWriter myStreamWriter = gitProcess.StandardInput)
                    {
                        myStreamWriter.WriteLine(command);
                    }
                    string streamReader = gitProcess.StandardOutput.ReadToEnd();
                    if (streamReader.Length == 0)
                    {
                        string error = gitProcess.StandardError.ReadToEnd();
                        return Task.FromResult(WorkResult.Failed(error));
                    }
                    return Task.FromResult(WorkResult.Success(streamReader));
                }
            }
            return Task.FromResult(WorkResult.Failed("Not valid command/folder path. Please edit"));
        }
    }
}