﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using IDAL;
using IDAL.Interfaces;
using IDAL.Models;

namespace BLL.Services
{
    public class ConcreteLoger : ILogerManager
    {
        private readonly ICommandResultDAO _commandResult;

        public ConcreteLoger(ICommandResultDAO commandResult)
        {
            _commandResult = commandResult;
        }

        public async Task<WorkResult> AddNewResult(CommandResult commandResult)
        {
            if (commandResult != null)
            {
                try
                {
                    WorkResult work = await _commandResult.AddNewResult(commandResult);
                    return work;
                }
                catch (Exception ex)
                {
                    return WorkResult.Failed(ex.Message);
                }
            }
            return WorkResult.Failed("Argument(commandResult) cannot be null");
        }

        public Task<List<CommandResult>> GetLast20Result()
        {
            return _commandResult.GetLast20Result();
        }
    }
}