How to demo:

1) You must edit

* Path to the git bash. For example <add key="gitPath" value="C:\\Git\\bin\\sh.exe" />

* Path to root folder (Where do you want that user will be created ) For example <add key="pathToTootFolder" value="C:\\Repositories\\" />

* Path to you Database in Connection.config

2) Build solution and copy:

* \DAL\bin\ DAL.dll, DAL.dll.config to \Myskiv_Git\bin

* \DAL\Connection.config to \Myskiv_Git\bin

3) Run Myskiv_Git project and try to invoke command

For example

The Command Line: git --help

root\: myRepository\myProjectName

![2016-05-25_10-27-22.jpg](https://bitbucket.org/repo/raKGXn/images/364957444-2016-05-25_10-27-22.jpg)
![2016-05-25_10-28-22.jpg](https://bitbucket.org/repo/raKGXn/images/85843806-2016-05-25_10-28-22.jpg)
![2016-05-25_10-29-20.jpg](https://bitbucket.org/repo/raKGXn/images/507639127-2016-05-25_10-29-20.jpg)
![2016-05-25_12-15-49.jpg](https://bitbucket.org/repo/raKGXn/images/1246915694-2016-05-25_12-15-49.jpg)