﻿using System.Data.Entity;
using IDAL.Models;

namespace DAL
{
    public class DBContext : DbContext
    {
        public DBContext()
            : base("DBContextConnection")
        {
        }

        public virtual DbSet<CommandResult> Results { get; set; }
    }
}