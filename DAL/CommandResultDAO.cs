﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IDAL;
using IDAL.Interfaces;
using IDAL.Models;

namespace DAL
{
    public class CommandResultDAO : ICommandResultDAO
    {
        public async Task<WorkResult> AddNewResult(CommandResult result)
        {
            using (var context = new DBContext())
            {
                context.Results.Add(result);
                int count = await context.SaveChangesAsync();
                if (count > 0)
                {
                    return WorkResult.Success();
                }
                return WorkResult.Failed("Not saved! SaveChanges return 0");
            }
        }

        public Task<List<CommandResult>> GetLast20Result()
        {
            using (var context = new DBContext())
            {
                var results = (from i in context.Results select i)
                    .OrderByDescending(x => x.EventTime)
                    .Take(20).ToList();

                return Task.FromResult(results);
            }
        }
    }
}